<?php
defined('ABSPATH') || die('Not allowed');

add_action('init',function(){
	
	register_post_type( 'sermon' ,array(
		'label' => 'Sermons',
		'labels' => array(
			'name' => 'Sermons',
			'singular_name' => 'Sermon',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Sermon',
			'edit_item' => 'Edit Sermon',
			'new_item' => 'New Sermon',
			'view_item' => 'View Sermon',
			'search_items' => 'Search Sermons',
			'not_found' =>  'Nothing found',
			'not_found_in_trash' => 'Nothing found in Trash',
			'parent_item_colon' => 'Parent Sermon',
		),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_icon' => plugins_url('i/cross.gif', __FILE__ ),
		// 'capability_type' =>
		'supports' => array('title','thumbnail','excerpt','editor','custom-fields'),
		'taxonomies' => array('speaker'),
		'show_in_rest' => true,
		'rewrite' => array('slug' => 'sermon','with_front'=>true),
		'template' => [
			array( 'sermon-interface/sermon-date', [] ),
			array( 'core/audio', [] ),
			array( 'sermon-interface/bible-verse', [] ),
		],
		// 'template_lock' => 'all', // 'insert'
	));

	register_taxonomy( 'speaker', 'sermon',array(
		'label' => 'Speakers',
		'labels' => array(
			'name' => 'Speakers',
			'singular_name' => 'Speaker',
			'menu_name' => 'All Speakers',
			'all_items' => 'All Speakers',
			'edit_item' => 'Edit Speaker Information',
			'view_item' => 'View Speaker Information',
			'update_item' => 'Update Speaker Information',
			'add_new_item' => 'Add a Speaker',
			'new_item_name' => 'Name of new speaker',
			'search_items' => 'Search Speakers',
			'not_found' => 'No Speaker Found',
		),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'hierarchical' => true,
		'rewrite' => array('slug'=>'sermons/speaker'),
		'context' => 'high',
		'show_admin_column' => true,
		'show_in_rest' => true,
	));

	register_taxonomy( 'series', 'sermon',array(
		'label' => 'Series',
		'labels' => array(
			'name' => 'Series',
			'singular_name' => 'Series',
			'menu_name' => 'All Series',
			'all_items' => 'All Series',
			'edit_item' => 'Edit Series Information',
			'view_item' => 'View Series Information',
			'update_item' => 'Update Series Information',
			'add_new_item' => 'Add a Series',
			'new_item_name' => 'Name of new series',
			'search_items' => 'Search Series',
			'not_found' => 'No Series Found',
		),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'hierarchical' => true,
		'rewrite' => array('slug'=>'sermons/series'),
		'context' => 'high',
		'show_admin_column' => true,
		'show_in_rest' => true,
	));

	register_taxonomy( 'occasion', 'sermon', array(
		'label' => 'Occasion',
		'labels' => array(
			'name' => 'Occasions',
			'singular_name' => 'Occasions',
			'menu_name' => 'All Occasions',
			'all_items' => 'All Occasions',
			'edit_item' => 'Edit Occasion Information',
			'view_item' => 'View Occasion Information',
			'update_item' => 'Update Occasion Information',
			'add_new_item' => 'Add an Occasion',
			'new_item_name' => 'Name of new occasion',
			'search_items' => 'Search Occasions',
			'not_found' => 'No Occasions Found',
		),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'hierarchical' => true,
		'rewrite' => array('slug'=>'sermons/occasions'),
		// 'context' => 'high',
		'show_admin_column' => true,
		'show_in_rest' => true,
	));

	register_post_meta( 'sermon', 'passage', array(
		'show_in_rest' => true,
		'single' => true,
		'auth_callback' => '__return_true',
		'type' => 'string',
	));

	add_rewrite_rule(
		'sermons/(?:/page/([0-9]+))?$', 
		'index.php?post_type=sermon&paged=$matches[1]', 
		'top' 
	);

	// add_rewrite_rule(
	// 	'sermons/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})?$',
	// 	'index.php?post_type=sermon&year=$matches[1]&monthnum=$matches[2]&day=$matches[3]',
	// 	'top'
	// );

	// add_rewrite_rule(
	// 	'sermons/([0-9]{4})/([0-9]{1,2})?$',
	// 	'index.php?post_type=sermon&year=$matches[1]&monthnum=$matches[2]',
	// 	'top'
	// );

	// add_rewrite_rule(
	// 	'sermons/([0-9]{4})?$',
	// 	'index.php?post_type=sermon&year=$matches[1]',
	// 	'top'
	// );

	add_rewrite_rule(
		'sermons/series/([^&]+)/page/([0-9]+)',
		'index.php?post_type=sermon&series=$matches[1]&paged=$matches[2]',
		'top'
	);

	add_rewrite_rule(
		'sermons/speaker/([^&]+)/page/([0-9]+)',
		'index.php?post_type=sermon&speaker=$matches[1]&paged=$matches[2]',
		'top'
	);

	add_rewrite_rule(
		'sermons/book/([^&\/]+)(?:/page/([0-9]+))?/?$',
		'index.php?post_type=sermon&book=$matches[1]&paged=$matches[2]',
		'top'
	);
});

add_filter('query_vars', function($vars){
	$vars[] ='book';
	return $vars;
});

add_filter('pre_get_posts', function($query){
	if(isset($query->query_vars['book'])) {
		$book = '^' . $query->query_vars['book'] . ' ';
		$book = urldecode($book);
		$query->set('meta_key','passage');
		$query->set('meta_value', $book );
		$query->set('meta_compare','REGEXP');
		$query->set('post_type','sermon');
		// reset the query vars
		$query->parse_query_vars();
	}
	return $query;
});

add_filter('manage_edit-sermon_columns', function ($cols){
	$cols['sermon_passage'] = 'Sermon Passage';
	return $cols;
});

add_filter('manage_sermon_posts_custom_column', function($col, $postid){
	switch($col) {
		case 'sermon_passage' :
			$s = new Sermon($postid);
			echo $s->getPassage();
			break;
	}
}, 10, 2);

add_filter('the_content', function($content){
	static $usedForSermon = false;
	$post = get_post();

	if( $usedForSermon )
		return $content;

	if( ! is_singular() || $post->post_type != 'sermon' )
		return $content;

	// Backwards-compat. 
	// @TODO This would also catch a modern sermon block collection, in which
	// the bible verse has been removed.
	$usedForSermon = true;
	if( ! has_block('sermon-interface/bible-verse', $post ) ) 
		return SermonsManager::printSingleSermon($post, array('uses-blocks' => false) );

	return SermonsManager::printSingleSermon($post);

}, 5); // before blocks are parsed out
