
// import { debounce } from 'lodash';

import BibleIcon from '../BibleIcon';

import { 
	parseBibleBook, 
	getBibleBookLink,
} from './lib';

import {
	TextControl,
	Button,
	Placeholder,
	PanelBody,
	ToggleControl,
} from '@wordpress/components';

import {
	useBlockProps,
	InspectorControls,
} from '@wordpress/block-editor';

import {
	useState,
	Fragment,
} from '@wordpress/element';

import {
	useDispatch,
	useSelect,
} from '@wordpress/data';

import apiFetch from '@wordpress/api-fetch';


export default function edit(props) {
	const blockProps = useBlockProps({
		className: "bible-verse-console"
	});

	const [ isLoading, setLoading ] = useState(0);
	const { createNotice, removeNotice } = useDispatch('core/notices');

	const { attributes, setAttributes, isSelected, } = props;

	const settings = useSelect( select => select('core').getSite() );

	const getBibleBookLink = function( book ) {
		// book = _tryForString(book);
		if( ! book ) return false;

		book = book.toLowerCase( book );
		//                   allow a space
		book = book.replace(/[^a-z0-9- ]+/g, '' );

		const url = (settings) ? settings.url : '';

		// var url = (typeof sermonInterface.siteUrl !== 'undefined' )
		// 	? sermonInterface.siteUrl
		// 	: '/';
		return `${url}/sermons/book/${book}`;
	};


	const getBibleText = function(passage) {

		removeNotice('esv-connection-error');

		if( ! passage ) {
			setAttributes( { bibleText: '' } );
			return;
		}

		setLoading(1);
		apiFetch({
			path: `/sermon-interface/v1/passage/`,
			method: 'POST',
			data: { passage },
		}).then( r => {

			if( r === false ) {
				createNotice( 'warning' , // success|info|warning|error
					'Nothing returned from the ESV. It could be a network issue or your passage wasn\'t found.',
					{
						id: 'esv-connection-error',
						isDismissible: true,
					}
				);
				// can't set it to false
				setAttributes({ bibleText: "" });
			} else {
				setAttributes({ bibleText: r });
			}

		}).catch( err => {
			console.error( err );
		}).finally( () => {
			setLoading(0);
		});
	};

	const updatePassage = function(passage){
		var bibleBook = parseBibleBook( passage );
		var bookLink = getBibleBookLink(bibleBook);
		setAttributes({
			passage,
			passageClone: passage,
			bookLink
		});
	};

	const passage = attributes.passageClone;

	const getBibleTextForm = (
		<div className="passage-intake" >
			<TextControl
				label="Bible Passage"
				value={ passage }
				onChange={ updatePassage  }
			/>
			{ passage && (
				<Button
					isPrimary={ true }
					isBusy={ isLoading }
					onClick={ () => getBibleText(passage) }
					href="#"
				>Get bible text from ESV</Button>
			)}

		</div>
	);

	const bibleBook = parseBibleBook( passage );

	const otherSermonsLink = (bibleBook && attributes.displayLinkToBookTaxonomy) ? (
		<p className="sermon-other-from-book">
			<a href={ attributes.bookLink }>Other sermons from { bibleBook }</a>
		</p>
	) : false;

	return (
		<section {...blockProps}>

			{ (!attributes.bibleText) && (
				<Placeholder
					icon={ BibleIcon }
					label="Bible Text"
					instructions="Enter the bible reference, then get the full ESV bible text."
				>
					{ getBibleTextForm }
				</Placeholder>
			)}

			{ (isSelected && attributes.bibleText) && getBibleTextForm }
			
			{ passage && (
				<Fragment>
					<h3 className="sermon-passage">{ passage }</h3>
					{ otherSermonsLink }
				</Fragment>
			)}
			
			{ attributes.bibleText && (
				<div className="sermon-bible-text" 
					dangerouslySetInnerHTML={{
						__html: attributes.bibleText,
					}}
				/>
			)}

			<InspectorControls>
				<PanelBody title="Other Settings">
					<ToggleControl
						label="Display link to other sermons in the same book"
						checked={ attributes.displayLinkToBookTaxonomy }
						onChange={ displayLinkToBookTaxonomy => setAttributes({displayLinkToBookTaxonomy}) }
					/>
				</PanelBody>
			</InspectorControls>
			
		</section>
	);
};
