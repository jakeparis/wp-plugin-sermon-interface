
import edit from './edit';
import save from './save';
// import BibleIcon from '../BibleIcon';

import {
	registerBlockType,
} from '@wordpress/blocks';


registerBlockType( 'sermon-interface/sermon-date', {
	title: 'Sermon Date',
	description: 'Displays the date of a sermon',
	keywords: [],
	// icon: "calendar",
	category: 'embed',
	apiVersion: 2,
	attributes: {},
	supports: {
		html: false,
		inserter: false,
	},

	// transforms,
	// deprecated,

	edit,
	save,
});
