<?php

class SermonsManager {

static function getBooksOfTheBible(){
	$books = array('Genesis','Exodus','Leviticus','Numbers','Deuteronomy','Joshua','Judges','Ruth','1 Samuel','2 Samuel','1 Kings','2 Kings','1 Chronicles','2 Chronicles','Ezra','Nehemiah','Esther','Job','Psalms','Proverbs','Ecclesiastes','Song of Solomon','Isaiah','Jeremiah','Lamentations','Ezekiel','Daniel','Hosea','Joel','Amos','Obadiah','Jonah','Micah','Nahum','Habakkuk','Zephaniah','Haggai','Zechariah','Malachi','Matthew','Mark','Luke','John','Acts','Romans','1 Corinthians','2 Corinthians','Galatians','Ephesians','Philippians','Colossians','1 Thessalonians','2 Thessalonians','1 Timothy','2 Timothy','Titus','Philemon','Hebrews','James','1 Peter','2 Peter','1 John','2 John','3 John','Jude','Revelation');
	
	return $books;
}

static function getAllSeries(){
	return get_categories( array('taxonomy'=>'series') );
}

static function getAllSpeakers(){
	return get_categories( array('taxonomy'=>'speaker') );
}

static private function _getLinkForTerm($name,$termName,$getBy='name'){
	$term = get_term_by($getBy,$name,$termName);
	if( ! $term )
		return false;
	$termLink = get_term_link( $term, $termName );
	return $termLink;
}
static function getLinkToSeries($seriesName,$getBy='name'){
	return self::_getLinkForTerm($seriesName,'series',$getBy);
}
static function getLinkToSpeaker($speakerName,$getBy='name'){
	return self::_getLinkForTerm($speakerName,'speaker',$getBy);
}
static function getLinkToOccasion($term,$getBy='name'){
	return self::_getLinkForTerm($term,'occasion',$getBy);
}

static function getLinkToBibleBook($book){
	$site = get_option('home') . '/sermons/book/' .$book;
	return $site;
}

static function getBibleTextFromSource($passage, $args=[] ) {

	require_once SERMON_MANAGER_INTERFACE_PATH . 'class/esv-api.class.php';
	$esvapi = EsvApi::get_instance();

	return $esvapi->getVerses( $passage, 'html', $args );
}


static function getLatestSermon(){
	$latest = get_posts(array(
		'post_type' => 'sermon',
		'posts_per_page' => '1'
	));
	if( ! $latest )
		return false;
	return new Sermon($latest[0]);
}


static function getLatestSermonBlock($atts=[]){
	$s = self::getLatestSermon();
	if( ! $s )
		return false;
	// TODO add ability to control what fields display
	$atts = shortcode_atts( array(
		'display'=>'short',
		'with-video' => false,
	), $atts);

	$out .= '<div class="latest-sermon">';

	if($atts['display'] == 'short')
		$out .= self::printSingleSermonShort($s->id,array(
			'audio-player' => true,
			'with-video' => $atts['with-video'],
		));
	else
		$out .= $s->printSingleSermon($post);
	$out .= '</div>';

	return $out;
}

static function shortcode_printSingleSermon($atts){
	extract(shortcode_atts( array(
		'id' => false,
		'title' => false
	),$atts));
	if( !$id && ! $slug && ! $title )
		return 'Tell me what sermon to get. Either by "id" or "title"';

	if($id)
		return self::printSingleSermon($id);

	if($title){
		$page = get_page_by_title( $title, 'OBJECT', 'sermon' );
		if( is_null($page) )
			return 'No Sermon found with that title.';

		$id = $page->ID;
		return self::printSingleSermon($id);
	}

	return 'Error: No sermon specified. (use "id" or "title")';

}

static function shortcode_doSermonPage($atts){
	$atts = shortcode_atts(array(
		'with-search-box'=>'1'
	),$atts);

	if($atts['with-search-box']=='1')
		return self::doSermonPage();
	else
		return self::doSermonPage(false);

}


static function doSermonPage($showSearchBox=true) {

	if($showSearchBox)
		$out .= self::printSermonSearchBox();
	
	$out .= self::printSermonList();
	return $out;
}

static function printSermonListPaging($count){
	$page = (get_query_var('paged')) ? get_query_var('paged') : 1;

	$perpage = get_query_var('posts_per_page');

	if( $count < $perpage && $page < 2 )
		return '';

	$out = '<div class="sermons-list-paging">';
	if( $count >= $perpage )
		$out .='<a class="nav-link-older" href="'. get_next_posts_page_link() .'">Older</a>';
	if( $page > 1){
		$out .= '<a class="nav-link-reset" href="'. get_pagenum_link( '1' ) .'">Back to first page</a>';
		$out .='<a class="nav-link-newer" href="'. get_previous_posts_page_link() .'">Newer</a>';
	}
	$out .= '</div>';

	return $out;
}

static function printSermonList(){

	$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$perpage = 15;

	$query = array(
		'post_type' => 'sermon',
		'posts_per_page' => 15,
		'paged'=>$page
	);
	$query['tax_query'] = array();

	// text search only searches post_content, title
	if( isset($_GET['search-text']) ) {
		$query['s'] = sanitize_title_for_query( $_GET['search-text'] );
	}

	if( isset($_GET['search-book']) ) {
		$book = $_GET['search-book'];

		$query['meta_query'] = array(
			'relation' => 'OR',
			[
				'key' => 'passage',
				'value' => $book,
				'compare' => 'LIKE',
			],
			[
				'key' => '_passage',
				'value' => $book,
				'compare' => 'LIKE',
			],
		);
	}

	if( isset($_GET['search-speaker']) && $_GET['search-speaker'] != '') {
		$speaker = sanitize_title_for_query( $_GET['search-speaker'] );
		$query['tax_query'][] = array(
			'taxonomy' => 'speaker',
			'field' => 'slug',
			'terms' => $speaker
		);
	}

	if( isset($_GET['search-series']) && $_GET['search-series'] != '' ) {
		$series = sanitize_title_for_query( $_GET['search-series'] );
		$query['tax_query'][] = array(
			'taxonomy' => 'series',
			'field' => 'slug',
			'terms' => $series
		);
	}

	$sermons = get_posts($query);

	$out .= "<h3>Page {$page}</h3>";

	$out .= '<div class="sermons-list">';
	if( $sermons ) : foreach($sermons as $post) :
		$out .= self::printSingleSermonShort($post,array('date'=>true));
	endforeach; endif;
	$out .= '</div>';

	$out .= self::printSermonListPaging( count($sermons) );

	return $out;
}

static function printSermonSearchBox(){

	$form .= '<div id="sermon-interface-searchbox" class="sermon-interface-searchbox">

	<h2>Search</h2>';

	$form .= '
	<form method="get" action="" class="form-search-by-name-description">
		<label>By name or description</label>
		<div>
			<input type="text" value="'.$_GET['search-text'].'" name="search-text">
			<input id="textSearchSubmit" type="submit" value="go">
		</div>
	</form>
	';

	$form .='<p class="search-section-divider">or</p>';

	$form .= '
	<form method="get" action="" id="searchSeriesDropdown">
		<label>By Series</label>
		<select name="search-series" class="js-autosubmit">
		<option value="">--</option>';

		foreach( self::getAllSeries() as $ser) {
			$form .= '<option value="'.$ser->slug.'"'.selected($ser->slug,$_GET['search-series'],false).'>'.$ser->name.'</option>'."\n"; 
		}

		$form .= '</select>
	</form>
	';
	
	$form .= '
	<form action="" method="get" id="searchSpeakerDropdown">
		<label>By Speaker</label>
		<select name="search-speaker" class="js-autosubmit">
		<option value="">--</option>';
	
		foreach( self::getAllSpeakers() as $ser) {
			$form .= '<option value="'.$ser->slug.'" '.selected($ser->slug,$_GET['search-speaker'],false).'>'.$ser->name.'</option>'."\n";
		}
		
		$form .= '</select>
	</form>
	';

	$form .= '
	<form action="" method="get" id="searchBookDropdown">
		<label>By Bible text</label>
		<select name="search-book" class="js-autosubmit">
		<option value="">--</option>';
	
		foreach( self::getBooksOfTheBible() as $ser) {
			$form .= '<option value="'.$ser.'"'.selected($ser,$_GET['search-book'],false).'>'.$ser.'</option>'."\n"; 
		}
		
		$form .= '</select>
	</form>
	';
	
	$form .= '</div>';
	return $form;
}

/** Deprecated way to display sermons. This was before the block editor
 *  pre v4.0.0
 */
static function printSingleSermon($post=false, $args=[] ){

	$args = array_merge([
		'uses-blocks' => true,
	], $args);

	// TODO add ability to turn on/off display like in short listing
	$s = new Sermon($post);

	// remove first audio block
	if( $s->hasBlock( 'core/audio' ) ) {
		$blocks = $s->getBlocks();
		foreach($blocks as $index=>$block){
			if($block['blockName'] === 'core/audio' ){
				unset( $blocks[$index] );
				break;
			}
		}
		$content = serialize_blocks( $blocks );
	} else {
		// printSingleSermon can be hooked on the_content filter
		$content = apply_filters('the_content', $s->post->post_content);
	}

	$speaker = $s->getSpeaker();
	if($speaker){
		$speakerLink = self::getLinkToSpeaker($speaker);
		$speaker = '<p class="sermon-speaker"><a href="'.$speakerLink.'">'.$speaker.'</a></p>';
	}
	$series = $s->getSeries();
	if($series){
		$seriesLink = self::getLinkToSeries($series);
		$series = '<p class="sermon-series"><a href="'.$seriesLink.'">'.$series.'</a> <span class="sermon-series-description">series</span></p>';
	}
	$occasion = $s->getOccasion();
	if($occasion){
		$occasionLink = self::getLinkToOccasion($occasion);
		$speaker = '<p class="sermon-occasion"><a href="'.$occasionLink.'">'.$occasion.'</a></p>';
	}
	$book = $s->getBook();
	if( $book ){
		$bookLink = self::getLinkToBibleBook($book);
		$bookLinkHtml = "<p class=\"sermon-other-from-book\"><a href=\"{$bookLink}\">Other sermons from {$book}</a></p>";
	}
	$bibletext = $s->getBibleText();
	$passage = $s->getPassage();
	$date = $s->getDate();
	$image = $s->getImage();
	if($image)
		$image = '<div class="single-sermon-image">'.$image.'</div>';
	
	$soundFile = $s->getSoundFile();
	if( $soundFile ) {
		$soundFilePlayer = '<div class="audio-player">'.$s->getSoundFilePlayer().'</div>';
	}
	$soundFileDownloadLink = self::getSoundFileDownloadLink( $soundFile->ID );
	if($soundFileDownloadLink) {
		$soundFileDownloadLink = '<p class="sermon-download-link">
			<a href="'.$soundFileDownloadLink.'">Save/Download this sermon</a>
			</p>';
	}

	$deprecated_bibleTextSection = '';
	if( $args['uses-blocks'] !== true ) {
		$deprecated_bibleTextSection = <<<BIBLE
		<div class="single-sermon-texts single-sermon-content-column">
			<h3 class="sermon-passage">{$passage}</h3>
			{$bookLinkHtml}
			<div class="sermon-bible-text">{$bibletext}</div>
		</div>
BIBLE;
	}

	$sermonContent = <<<SERMON
		<div class="single-sermon-content-block">
			
			<div class="single-sermon-meta single-sermon-content-column">
				{$image}
				<p class="sermon-date">{$date}</p>
				{$series}
				{$speaker}
				{$soundFilePlayer}
				{$soundFileDownloadLink}
			</div>
			
			{$deprecated_bibleTextSection}

			<div class="single-sermon-content">
				{$content}
			</div>

		</div>

SERMON;

	return $sermonContent;
}


/**
 * print an html block for a short sermon listing
 * @param  mixed  $post        post object | post id (default to current global $post)
 * @param  array  $userinclude an array of elements to include or not include
 *                             in the output. The keys are the element names,
 *                             and the value is a boolean indicating whether
 *                             to display or not
 * @return string              
 */
static function printSingleSermonShort($post=false,$userinclude = array()){

	$include = array_merge(array(
		'title' => true,
		'speaker' => true,
		'series' => true,
		'date' => false,
		'audio-player' => false,
		'with-video' => false,
		'link' => true
	),$userinclude);

	$s = new Sermon($post);
	// display an embedded video

	$videoHtml = ($include['with-video'] && $include['with-video'] !== 'false')
		? $s->getEmbeddedVideo()
		: false;

	$out = '<div class="sermon-single-short-listing">';
	if( $include['title'] )
		$out .= '<p class="sermon-title">'.$s->getTitle().'</p>';

	if( $include['date'] || $include['series'] ) {

		if($include['date'])
			$date = '<span class="sermon-date">'.$s->getDate().'</span>';
		if($include['series'])
			$series = '<span class="sermon-series">'.$s->getSeries().'</span>';

		$out .= '<p class="sermon-meta">'.$date.$series.'</p>';
	}

	if( $include['speaker'] ) {
		$speakerLink = self::getLinkToSpeaker( $s->getSpeaker() );

		$out .= '<p class="sermon-speaker">'.$s->getSpeaker().'</p>';
	}

	if( $include['audio-player'] && $s->getSoundFilePlayer() )
		$out .= '<div class="audio-player">'.$s->getSoundFilePlayer().'</div>';

	$out .= $videoHtml;
	
	if( $include['link'] )
		$out .= '<p class="sermon-link">
			<a href="'.$s->getLink().'">Sermon text and details</a>
			</p>';
	
	$out .= '</div>';
	return $out;
}

static function getSoundFileDownloadLink($attId){
	$attId = (int) $attId;
	if( empty($attId) )
		return false;
	$url = get_option('home') . '?sermon-file-download=' . $attId;
	return $url;
}



} //end class
