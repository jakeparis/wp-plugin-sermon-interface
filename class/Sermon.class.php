<?php

class Sermon {

	var $post = false;
	var $meta = false;
	var $bibletext = false;
	var $id;
	var $soundFile = false;

	/** Pass in either a post id or a post object
	 */
	function __construct($post=false){

		if( $post===false )
			global $post;

		$this->post = get_post($post);
		if( $this->post == false) {
			return new WP_Error('cant construct sermon','That post object does not seem to exist');
		}
		$this->id = $this->post->ID;

		if( ! $this->post )
			return false;

	}

	function getId(){
		return $this->post->ID;
	}

	function getTitle(){
		$t = apply_filters('the_title',$this->post->post_title);
		return $t;
	}
	function getDate($format="F j, Y"){
		return date($format,strtotime($this->post->post_date));
	}
	function getDescription( $raw=false ){
		if( $raw === false )
			return $this->post->post_content;
		return apply_filters('the_content',$this->post->post_content);
	}
	function getExcerpt(){
		return get_the_excerpt( $this->post );
	}
	function getThumbnail(){
		return $this->getImage('thumbnail');
	}
	function getLink(){
		return get_permalink($this->id);
	}
	function getImage($size='full'){
		$fid = get_post_thumbnail_id($this->id);
		if( ! $fid )
			return false;
		$url = wp_get_attachment_image_url($fid,$size);
		return "<img src='$url'>";
	}

	/** Get the first audio block
	 */
	function getSoundFiles(){

		$attachments = array();

		if( has_block( 'core/audio', $this->post->post_content ) ) {
			$blocks = parse_blocks( $this->post->post_content );
			$audioBlocks = array_filter($blocks, function($block) {
				return $block['blockName'] === 'core/audio';
			});
			$firstAudioBlock = array_shift($audioBlocks);
			$id = $firstAudioBlock['attrs']['id']; 
			if( $id ) {
				$attachments = array( get_post( $id ) );
			}
		}

		// older way
		if( ! count($attachments) ) {
			$types = array('audio/mpeg','audio/ogg','audio/x-mpeg-3','audio/wav','audio/x-ms-wma');
			$attachments = get_children(array(
				'post_parent' => $this->id,
				'post_type'   => 'attachment',
				'numberposts' => -1,
				'post_mime_type' => $types,
			));
		}


		if( count($attachments) ){
			foreach($attachments as $key=>$att){
				if( $att instanceof WP_Post )
					$attachments[$key]->url = wp_get_attachment_url($att->ID);
			}
			return $attachments;
		}

		// try looking in the blocks
		

		return array();
	}

	function getSoundFile(){
		if( ! $this->soundFile ) {
			$files = $this->getSoundFiles();
			if( ! count($files) )
				return false;
			$this->soundFile = array_shift($files);
		}
		return $this->soundFile;
	}

	function getSoundFileUrl(){
		$file = $this->getSoundFile();
		if( !$file )
			return false;
		if( is_string($file) )
			return $file;
		return $file->url;
	}

	function getSoundFilePlayer(){
		// $player = wp_audio_shortcode(array(
		// 	'src' => $this->getSoundFileUrl(),
		// 	'loop' => false,
		// 	'autoplay' => false,
		// ) );
		$url = $this->getSoundFileUrl();
		if( ! $url )
			return false;

		$player = '<audio controls src="'.$url.'"></audio>';
		return $player;
	}

	function getSoundFileId(){
		$file = $this->getSoundFile();
		if( ! $file )
			return false;
		return $file->ID;
	}

	function getEmbeddedVideo() {
		$blocks = parse_blocks($this->post->post_content);
		foreach($blocks as $b) {
			if( stripos( $b['blockName'], 'core-embed' ) === false )
				continue;
			if( $b['attrs']['type'] !== 'video' )
				continue;
			return apply_filters('the_content', render_block($b) );
		}
		return false;
	}

	function getEditLink(){
		return get_edit_post_link($this->id);
	}

	private function _getMetaFromDb(){
		$this->meta = get_post_meta( $this->id );
	}

	function getMeta($field){
		if( ! $this->meta )
			$this->_getMetaFromDb();
		if( ! array_key_exists($field, $this->meta) )
			return '';
		return $this->meta[$field][0];
	}
	function setMeta($field, $val){
		update_post_meta( $this->id, $field, $val );

		if( ! $this->meta )
			return;

		$this->meta[$field] = array($val);

	}

	private function _getPassageFromMeta(){
		$p = $this->getMeta('_passage');
		if( ! $p )
			$p = $this->getMeta('passage');
		return $p;
	}

	function getBook(){
		$p = maybe_unserialize( $this->_getPassageFromMeta() );
		if( ! $p )
			return;
		// backwards-compat. < 2.3.0 Not stored as array any more
		if( is_array($p) )
			return $p['book'];

		preg_match('@^([123]? ?[a-zA-Z ]+)@',$p, $matches);
		if( isset($matches[1]) )
			return $matches[1];
		return '';
	}

	function getPassage(){

		$p = maybe_unserialize( $this->_getPassageFromMeta() );

		// if we've stored a string, return a string
		if( is_string($p) )
			return $p;

		// Backwards-compat. < 2.3.0
		// otherwise turn the stored array into a formatted string
		$b = $p['book'];
		$c = $p['chapter'];
		$v = $p['verse'];

		$out = $b;
		if($c) {
			$out .= " " . $c;
			if($v)
				$out .= ":";
		}
		if($v)
			$out .= $v;

		return $out;
	}

	/**
	 * Deprecated: method to get bible text stored as meta field
	 * @return  string  html of bible text or false
	 */
	function getBibleText(){
		$bt = $this->getMeta('bible-text');
		if( $bt )
			$bt = balancetags($bt,true);
		return $bt;
	}

	function getSpeaker($num=0){
		$speaker_obj = get_the_terms( $this->id, 'speaker' );
		if( ! $speaker_obj)
			return false;
		return $speaker_obj[$num]->name;
	}
	function getSeries($num=0){
		$series_obj = get_the_terms( $this->id, 'series' );
		if( ! $series_obj)
			return false;
		return $series_obj[$num]->name;
	}
	function getOccasion($num=0){
		$series_obj = get_the_terms( $this->id, 'occasion' );
		if( ! $series_obj)
			return false;
		return $series_obj[$num]->name;
	}

	function hasBlock( $blockSlug ){
		return has_block( $blockSlug, $this->post );
	}
	function hasBlocks() {
		return has_blocks( $this->post );
	}
	function getBlocks() {
		if( ! $this->hasBlocks() )
			return false;
		return parse_blocks( $this->post->post_content);
	}
	function getBlock( $blockSlug ) {
		$blocks = $this->getBlocks();
		if( ! $blocks) 
			return false;
		$matches = array_filter($blocks, function($block) use ($blockSlug) {
			return $block['blockName'] == $blockSlug;
		});
		return array_shift($matches);
	}

	private function setTaxonomy($taxname,$val){
		// get slug or id of term
		if( is_integer($val) )
			$getBy = 'id';
		else
			$getBy = 'slug';

		$term = get_term_by( $getBy , $val , $taxname );

		// term doesn't exist yet
		if( ! $term ) {

			$newTerm = wp_create_term( $val, $taxname );
			if( ! is_wp_error($newTerm) )
				$term_id = (int) $newTerm['term_id'];

		} else {
			$term_id = (int) $term->term_id;
		}

		wp_set_object_terms( $this->id, $term_id, $taxname ); // add 4th param of true to append rather than overwrite terms

	}

	function setPassage($val){
		$this->setMeta( 'passage', $val );
	}
	
	function setSeries($val){
		$this->setTaxonomy('series',$val);
	}
	function setSpeaker($val){
		$this->setTaxonomy('speaker',$val);
	}
	function setOccasion($val){
		$this->setTaxonomy('occasion',$val);
	}

	function setDate($date, $isTimestamp=false){
		if( ! $isTimestamp )
			$date = strtotime($date);
		else
			$date = (int) $date;
		$date = date("Y-m-d G:i:s", $date);
		if( $date == $this->post->post_date )
			return;

		wp_update_post(array(
			'ID' => $this->id,
			'edit_date' => true,
			'post_date' => $date
		));
		$this->post->post_date = $date;
	}



}